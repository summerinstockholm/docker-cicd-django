# Настройка простого CI/CD

Не такая уж и большая инструкция как настроить простой CI/CD в GitLab
Имеется джанго проектик с БД на sqllite. Имеем докерфайл, .gitlab-ci.yml, init.sh и сами .py файлы проекта.

1. В отличии от дефолтного джанго герлс - в settings.py в ALLOWED_HOSTS добавлено '*'<br/>
2. Учтен момент с миграциями БД в скрипте init.sh для того чтобы деплой не падал при повторном его запуске.<br/>
3. В .gitlab-ci.yml описаны простенькие три стейджа: тесты, билд и деплой.<br/>
4. Сбилженный образ пушится (и хранится) в собственное реджистри gitlab. От того и `docker push ${CI_REGISTRY_IMAGE}:${tag}` в .gitlab-ci.yml файлике.<br/>
5. Проджект раннеры смотрим в /название_проекта/-/settings/ci_cd. На раннере(ах) необходимо установить докер:<br/>
`sudo apt update`<br/>
`sudo apt install apt-transport-https ca-certificates curl software-properties-common`<br/>
`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`<br/>
`sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"`<br/>
`sudo apt update`<br/>
`apt-cache policy docker-ce`<br/>
`sudo apt install docker-ce`<br/>
гитлаб раннер (используя официальный гитлаб репозиторий):<br/>
`curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash`<br/>
`sudo apt-get install gitlab-runner`<br/>
а затем зарегистрировать проджект раннер (в качестве экзекьютора - докер):<br/>
`sudo gitlab-runner register`<br/>
`Enter the GitLab instance URL`<br/>
`https://gitlab.com`<br/>
`Enter the registration token:`<br/>
вводить токен из /название_проекта/-/settings/ci_cd<br/>
`Enter a description for the runner:`<br/>
пишем например ya-docker-vm<br/>
`Enter tags for the runner (comma-separated):`<br/>
пишем docker<br/>
`Enter optional maintenance note for the runner:`<br/>
ничего не пишем<br/>
`Enter an executor:`<br/>
пишем docker<br/>
`Enter the default Docker image:`<br/>
пишем базовый докер образ - python:3-alpine<br/>
На этом раннер для докера зарегестрирован.<br/>
Регистрируем проджект раннер в качестве экзекьютора будет shell:<br/>
`sudo gitlab-runner register`<br/>
`Enter the GitLab instance URL`<br/>
`https://gitlab.com`<br/>
`Enter the registration token:`<br/>
вводить токен из /название_проекта/-/settings/ci_cd<br/>
`Enter a description for the runner:`<br/>
пишем например ya-shell-vm<br/>
`Enter tags for the runner (comma-separated):`<br/>
пишем shell<br/>
`Enter optional maintenance note for the runner:`<br/>
ничего не пишем<br/>
`Enter an executor:`<br/>
пишем shell<br/>
В /название_проекта/-/settings/ci_cd в проджект раннерах должны появиться два раннера.<br/>
В группу докер добавить пользователя gitlab-runner.<br/>
`sudo adduser gitlab-runner docker`<br/>
 
# Complete Django Girls Tutorial

This repository contains the code that one would eventually have were they to go through the [Django Girls tutorial](https://tutorial.djangogirls.org/en/).

[![CircleCI](https://circleci.com/gh/NdagiStanley/django_girls_complete.svg?style=svg)](https://circleci.com/gh/NdagiStanley/django_girls_complete)

## Differences

Expressing my authorial rights, some things are a bit different from the tutorial:

- A `Log in` and `Log out` links on the page header
- A `Back` link within the *blog-detail* and *blog-edit* pages
- A more extensive `.gitignore` file
- A `.editorconfig` file
- An additional python package in the requirements.txt: `pycodestyle`

- Within `mysite/settings.py`,

  - Use of `Africa/Nairobi` as my *TIME_ZONE*
  - Use of `en-us` as my *LANGUAGE_CODE*
  - Addition of `0.0.0.0` and `.herokuapp.com` to the *ALLOWED_HOSTS* list

## Setup

In a python virtual environment, run:

- `pip install -r requirements.txt`
- `python manage.py migrate blog`
- `python manage.py createsuperuser` (to create user that you'll use to log in)

### Run the application

```bash
python manage.py runserver
```

Now, you are good to go. Your blog is ready.

### Test

```bash
python manage.py test
```

The application will be live at [0.0.0.0:8000](0.0.0.0:8000)

### Log in/ out

- Click on `Log in` (you'll be redirected to the Admin page)
- On the admin page, fill in the credentials of the superuser created in [Setup](#setup)
- Click on the *Log in* button (You'll be redirected back to the page)
- Click on `Log out` to log out.

### Blog entry

- Log in
- Click on the `+` button, enter the _**title**_ and _**text**_
- Finally hit the `Save` button
